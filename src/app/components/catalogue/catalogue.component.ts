
import { Component, OnInit, Input  } from '@angular/core';
import {SearchService} from 'src/app/services/stock/search/search.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';


@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {

  @Input() bookDeleted;
  books: any[]=[];

  // Declarations for the search filter

  filterBooks: boolean = false;
  searchText:string;

  constructor(private session: SessionService, private router: Router,private searchService:SearchService) { }

  
  get user() {
    return this.session.get().user;
  };
  get username() {
    return this.session.get().username;
  };
  get lastLogin() {
    return this.session.get().lastLogin;
  }


  async ngOnInit() {

    if (this.session.get() !== false) {
      this.router.navigateByUrl('/catalogue');
    }


    try {
      // On init a get request is performed in order to visualise the book-store book list
      const result: any = await this.searchService.getBooks();
      this.books=result.data || this.bookDeleted;
      /* this.books=this.bookDeleted; */
      console.log(result);

    } catch (e) {

    }
  }

  ///// Add book click event

  onAddBookClicked(){
    this.router.navigateByUrl('/addbook');

  }

  ///// Edit user click event

  onEditClicked(){
    this.router.navigateByUrl('/edituser');

  }

  ///// Logout click event

  onLogoutClicked(){
    
    this.session.remove();
    this.router.navigateByUrl('/login')
  }

  ///// Filter checkbox
  
  async onFilterClicked() {
    
    this.filterBooks = !this.filterBooks;
    try {
      const result: any = await this.searchService.getBooks();
      this.books=result.data || [];
      console.log(result);
      console.log(this.filterBooks);

      if (this.filterBooks === true) {
         this.books = this.books.filter(function (el) {
          return el.stock > 0 ;
          
        });
      }

    } catch (e) {

    } finally {
      
    }
  };

 async onDeleteClicked() {

  if (confirm("Are you sure you want to delete book information?")) {
    try {
      const result: any = await this.searchService.getBooks();
      this.books=result.data
      console.log(result);
    } catch (e) {
  
  }
  } else { 
    
  }
  
  
}

}
