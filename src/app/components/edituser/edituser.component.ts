import { Component, OnInit,  Output, EventEmitter } from '@angular/core';
import {  FormGroup, FormControl, Validators} from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router} from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {

  @Output () editEvent: EventEmitter<string> = new EventEmitter();

  /* Username validators: username must be between 8 and 20 characters long */
  
  edituserForm: FormGroup = new FormGroup ({
    username: new FormControl('',[Validators.required,
                                  Validators.minLength(8),
                                  Validators.maxLength(20)])
    })

    isUpdating:boolean = false;
    edituserError:string;
    initialValue:string;
    email='';

  constructor(private authService: AuthService, private router:Router, private session:SessionService) { }

  get username() {
    return this.edituserForm.get('username');
  }


  ngOnInit(): void {
    this.email=this.session.get().username;
  }

  ///// Cancel click event

  onCancelClicked() {

    if (confirm("Cancel editing user (if yes you'll be redirected to the catalogue page)")) {
      this.router.navigateByUrl('/catalogue');
    } else {
      
    }
}
  
  ///// Update user click event

  async onUpdateClicked() {

    this.edituserError = '';

    try {
      this.isUpdating = true;
        if (this.edituserForm.valid===true) {
          const result: any = await this.authService.editUser(this.edituserForm.value);
          console.log(result);
          if(result.status < 400) {
            this.session.save({
              token: this.session.get().token,
              user:result.data.full_name,
              username: result.data.email,
              lastLogin: result.data.last_login
            });
            this.router.navigateByUrl('/catalogue');
          }

        }
        else if (this.username.valid===false) {
          alert("Please fill in the title field correctly");
        }
        else {
          alert('Fill in the fields properly');
        }

      } catch (e) {
        
        console.log(e);
        this.edituserError = e.error.error;
        //this.usernameReset = false;
        this.edituserForm.reset(this.initialValue);

    } finally {
      this.isUpdating = false;
    }

  }

}
