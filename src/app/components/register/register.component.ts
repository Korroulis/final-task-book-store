import { Component, OnInit,  Output, EventEmitter } from '@angular/core';
import {  FormGroup, FormControl, Validators} from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router} from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @Output () signupEvent: EventEmitter<string> = new EventEmitter();
  
/*----- Validators (
        - username length between 8 and 20 letters
        - email length between 8 and 40 letters
        - password length between 9 and 20 letters
        - password must contain at least one lower and uppercase letter as well as a number and a special character
        - password and confirm password must be identical

) */

  registerForm: FormGroup = new FormGroup ({
    username: new FormControl('',[Validators.required,
                                  Validators.minLength(8),
                                  Validators.maxLength(20)]),
    email: new FormControl('',[Validators.required,
                                Validators.minLength(8),
                                Validators.maxLength(40)]),
    password: new FormControl('',[Validators.required,
                                  Validators.minLength(9),
                                  Validators.maxLength(20),
                                  Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]),
    confirmPassword: new FormControl('',[Validators.required])
  }, this.passwordMatching);

  passwordMatching(frm: FormGroup) {
    return frm.get('password').value === frm.get('confirmPassword').value
       ? null : {'mismatch': true};
};

isRegistering:boolean = false;
registerError:string;
usernameReset:boolean = true;
initialValue:string;

  constructor(private authService: AuthService, private router:Router, private session:SessionService) { }

  get username() {
    return this.registerForm.get('username');
  }

  get email() {
    return this.registerForm.get('email');
  }

  get password() {
    return this.registerForm.get('password');
  }

  get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  }

  ngOnInit(): void {
    
    if (this.session.get() !== false) {
      this.router.navigateByUrl('/catalogue');
    }
  }

  /* ----- Signup button logic: If all the criteria for the username email and password
  are met and the passwords match each other then redirect to the catalogue page  ----- */

  async onRegisterClicked() {
  
    this.initialValue = '';
    this.registerError = '';
    
    try {
      this.isRegistering = true;
        if (this.registerForm.valid===true) {
          const result: any = await this.authService.register(this.registerForm.value);
          console.log(result);
          if (result.status<400) {
            this.session.save({
              token: result.data.token,
              user:result.data.user.full_name,
              username: result.data.user.email,
              lastLogin: result.data.user.last_login
            });
            this.router.navigateByUrl('/catalogue');
          }
        }
        else if (this.username.valid===false) {
          alert("Please insert a correct username");
        }
        else if (this.email.valid===false) {
          alert("Please insert a correct email");
        }
        else if (this.password.valid===false) {
          alert("Please insert a correct password");
        }
        else if (this.password.value!==this.confirmPassword.value) {
          alert("Password and Confirm Password do not match");
        } else {
          alert('Fill in the fields properly');
        }

      } catch (e) {
        
        this.isRegistering = false;
        this.registerError = e.error.error;
        this.usernameReset = false;
        this.registerForm.reset(this.initialValue);
      }
  }

}
