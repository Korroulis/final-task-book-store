import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-book-item',
  templateUrl: './book-item.component.html',
  styleUrls: ['./book-item.component.css']
})
export class BookItemComponent implements OnInit {

  @Input() book;
  @Output() bookDeleted: EventEmitter<any> = new EventEmitter();


  isDeleting:boolean = false;
  deleteBookError:string;
  

  constructor(private router:Router, private authService: AuthService, private session: SessionService) { }

  ngOnInit(): void {
    
      

    
  }

  ///// Delete click event

  async onDeleteClicked() {

    this.deleteBookError = '';

    try {
      this.isDeleting = true;
      const result: any = await this.authService.deleteBook(this.book);
      if (result.status<400) {

        this.bookDeleted.emit(result.data);

      }
    
  } catch(e) {
    console.log(e);
    this.deleteBookError = e.error.error;

  } 
}
}
