import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  isLoggedIn: boolean;
  constructor(private auth:AuthService, private router: Router, private session:SessionService) {

   }
   

  ngOnInit():  void {
    if (this.session.get() !== false) {
      this.isLoggedIn = true; 
  }

  }
}
