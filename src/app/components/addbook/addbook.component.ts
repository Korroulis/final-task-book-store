import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {  FormGroup, FormControl, Validators} from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router} from '@angular/router';


@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})
export class AddbookComponent implements OnInit {

  @Output () addBookEvent: EventEmitter<string> = new EventEmitter();

/*----- Validators (
        - title max length: 100 letters
        - author min length: 1 letter, max length: 50 letters
        - description: min length 2 letters, max length: 700 letters
        - ISBN must be a 13 digits number
        - URL of cover image must be between 7 and 200 characters long
        - stock number cannot be more than 10 digits

) */

  addbookForm: FormGroup = new FormGroup ({
    title: new FormControl('',[Validators.required,
                                  Validators.maxLength(100)]),
    author: new FormControl('',[Validators.required,
                                Validators.minLength(1),
                                Validators.maxLength(50)]),
    description: new FormControl('',[Validators.required,
                                  Validators.minLength(2),
                                  Validators.maxLength(700)]),
    isbn: new FormControl('',[Validators.required,
                                  Validators.minLength(13),
                                  Validators.maxLength(13),
                                  Validators.pattern('^[0-9]+$')]),
    urlToCover: new FormControl('',[Validators.required,
                                    Validators.minLength(7),
                                    Validators.maxLength(200)]),
    stock: new FormControl('',[Validators.required,
                                      Validators.maxLength(10)]),

});


isAdding:boolean = false;
registerBookError:string;
usernameReset:boolean = true;
initialValue:string;
registerSuccess:boolean = false;

  constructor(private authService: AuthService, private router:Router) { }

  get title() {
    return this.addbookForm.get('title');
  }

  get author() {
    return this.addbookForm.get('author');
  }

  get description() {
    return this.addbookForm.get('description');
  }

  get isbn() {
    return this.addbookForm.get('isbn');
  }

  get urlToCover() {
    return this.addbookForm.get('urlToCover');
  }

  get stock() {
    return this.addbookForm.get('stock');
  }

  ngOnInit(): void {

  }

  ////////  Addbook event

  async onAddClicked() {

    this.registerBookError = '';

    try {
      this.isAdding = true;
        if (this.addbookForm.valid===true) {
          const result: any = await this.authService.addBook(this.addbookForm.value);
          console.log(result);
          if (result.status<400) {
            this.registerSuccess = true;
          }
        }
        else if (this.title.valid===false) {
          alert("Please fill in the title field correctly");
        }
        else if (this.author.valid===false) {
          alert("Please fill in the author field correctly");
        }
        else if (this.description.valid===false) {
          alert("Please fill in the description field correctly");
        }
        else if (this.isbn.valid===false) {
          alert("Please fill in the ISBN field correctly");
        }
        else if (this.urlToCover.valid===false) {
          alert("Please fill in the URL to cover field correctly");
        }
        else if (this.stock.valid===false) {
          alert("Please fill in the stock field correctly");
        } else {
          alert('Fill in the fields properly');
        }

      } catch (e) {
        
        console.log(e);
        this.registerBookError = e.error.error;
        this.usernameReset = false;
        this.addbookForm.reset(this.initialValue);
        this.registerBookError = e.error.error;
    } finally {
      this.isAdding = false;
     
    }

  }

  ////// Cancel button

  onCancelClicked() {
      if (confirm("Cancel adding book (if yes you'll be redirected to the catalogue page)")) {
        this.router.navigateByUrl('/catalogue');
      } else {
        
      }
  }

  ////// Alert OK click event

  onOkClicked() {
    this.router.navigateByUrl('/catalogue');
  }

}
