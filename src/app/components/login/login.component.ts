// Imports

import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { AuthService } from 'src/app/services/auth/auth.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {
  // Explain the X it's not a good choosing of name 
  [x: string]: any;

  public loginTitle = "Login to your account";
  
/*----- Validators (
        - username length between 8 and 20 letters
        - password length between 9 and 20 letters

) */

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('',[Validators.required,
                                  Validators.minLength(8),
                                  Validators.maxLength(20)]),
    password: new FormControl('',[Validators.required,
                                  Validators.minLength(9),
                                  Validators.maxLength(20)])
  });

  isLoading:boolean = false;
  loginError:string;
//by the code here we are checking if there is some token stored in the services.session.ts
constructor(private session:SessionService, private router: Router, private auth:AuthService) { 
    if (this.session.get() !== false){
      this.router.navigateByUrl('/catalogue');
    }
  }


  ngOnInit(): void {
  }

  get username (){
    return this.loginForm.get('username')
  }

  get password() {
    return this.loginForm.get('password')
  }

  ///// Login click event

  async onLoginClicked(){

    this.loginError = '';

    try {
      this.isLoading = true;
      const result:any =await this.auth.login(this.loginForm.value);
      console.log(result);

      if(result.status < 400) {
        this.session.save({token: result.data.token, user:result.data.user.full_name, username: result.data.user.email, lastLogin: result.data.user.last_login });
        this.router.navigateByUrl('/catalogue');
      }

    }catch(e) {
      this.loginError = e.error.error;
    } finally {
      this.isLoading = false;
    }
  };
}