// Import modules

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import {  CatalogueComponent } from './components/catalogue/catalogue.component';


// Import components

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AboutComponent } from './components/about/about.component';
import { AddbookComponent } from './components/addbook/addbook.component';
import { EdituserComponent } from './components/edituser/edituser.component';
import { TokenInterceptor } from './interceptors/token/token.interceptor';
import { FilterPipe } from './components/filter.pipe';
import { BookItemComponent} from './components/book-item/book-item.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    AboutComponent,
    CatalogueComponent,
    BookItemComponent,
    AddbookComponent,
    EdituserComponent,
    FilterPipe
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
