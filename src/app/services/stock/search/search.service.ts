import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SessionService } from '../../session/session.service';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  
  constructor(private http:HttpClient, private session: SessionService) { }

  getBooks(): Promise<any> {
    return this.http.get(`${environment.apiUrl}/v1/api/books`).toPromise();
  }
}
