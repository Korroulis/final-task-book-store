import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})



export class SessionService {
  constructor() { }
  
  ///// Create a new session promise

  save(session:any) {
    localStorage.setItem('session',JSON.stringify(session));
  }

  ///// Get session promise

  get(): any {
    const savedSession = localStorage.getItem('session');
    return savedSession ? JSON.parse(savedSession) : false;
  }
  
  ///// Remove session promise

  remove(): any {
    localStorage.removeItem('session');
  }
  
}
