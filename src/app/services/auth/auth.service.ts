import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SessionService } from '../session/session.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  [x: string]: any;

  constructor(private http: HttpClient, private router: Router, private session:SessionService) { }

  // Register user promise

  register(user): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/register`, {
      //user: {...user}
      user: {
        email: user.email,
        full_name: user.username,
        password: user.password
    }
    }).toPromise();
  }

  ///// Edit user promise

  editUser(user): Promise<any> {
    return this.http.patch(`${environment.apiUrl}/v1/api/users`, {
      user: {
        full_name: user.username
    }
    }).toPromise();
  }

  ///// Add book promise

  addBook(book): Promise<any> {
    console.log(book);
    return this.http.post(`${environment.apiUrl}/v1/api/books`, {
      
      book: {
        title: book.title,
        authors: book.author,
        cover: book.urlToCover,
        description: book.description,
        stock: book.stock,
        isbn: book.isbn
    }
    }).toPromise();
  }

  ///// Delete book promise

  deleteBook(book): Promise<any> {
    return this.http.delete(`${environment.apiUrl}/v1/api/books/${book.id}`, {
    }).toPromise();
  }

  ///// Login promise 
  
  login(user): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/login`, {
      user: {
        email: user.username,
        password: user.password
    }
    }).toPromise()
  }

  ///// Check if user is logged in (session exists?)
  
  public isLoggedIn(): boolean {
    //return this.user !== null;
    if (this.session.get() !== false) {
      return true;
    } else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }
}
