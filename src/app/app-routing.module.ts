// Import modules

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

// Import components

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { CatalogueComponent} from './components/catalogue/catalogue.component';
import { AboutComponent } from './components/about/about.component';
import { EdituserComponent } from './components/edituser/edituser.component';
import { AddbookComponent } from './components/addbook/addbook.component';



const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'catalogue',
    //loadChildren: ()=> import('./components/catalogue/catalogue.module').then(m=>m.CatalogueComponentModule),
    component: CatalogueComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'edituser',
    component: EdituserComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'addbook',
    component: AddbookComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    pathMatch:'full',
    redirectTo:'/login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
