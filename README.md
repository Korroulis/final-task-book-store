# BookStore

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

## Project info

The Book Store Catalogue will only be used to view and manage stock in the Book Store. It will not be used as a sales platform therefor no sales will be made on this system.Anything other than Basic Authentication (Login, Register and Logout) and Adding, remove or editing books is out of the scope of this product.

### Structure

The Book store catalogue contains the following pages:

- **User login (public):**
The user is expected to navigate to the URL of the application. When a user is not logged in, they must be presented with the Login page. The user must proceed to fill in their username (email address) along with a matching password. The user will then click on the Login button once both the username and password have been filled in. 

- **User register (public):**
The user is expected to navigate to the Register page from the Login Page. If a user is Registered, they are expected to return to the Login page by clicking on the provided link (See wireframe). The user must proceed to fill in their Full name, username (email address) along with a matching password. There will also be a field to confirm the password to check for any typos in the original password. The user will then click on the Register button,once both the username and password have been filled in.

*Features*

1. Password strength: Length plus minimum requirement of at least one lower- and upprcase leter, as well as a number and special character

- **Catalogue: Book list page (protected):**

The Catalogue Page should contain the books in the storeas a list. The user should be able to search a book based on a title and author in a search bar.
There should be a checkbox where the user can show only books in stock.
The catalogue page must also show the currently logged in users’name, surname, email address and when the last login was.This should also provide a button to edit the profile a button to logout. 
It is expected that this page must be displayed after a successful login. The user will be presented with the list as the focus. 
If a user clicks in the search bar, once they start typing it should start filtering the results in the book catalogue list.
When a user clicks on the checkbox to show only in-stock books, the list should automatically be updated.
Under the profile section of the page, the user must be able click a button to edit their profile.There must also be a button for the user to logout. Clicking on the logout button should first prompt a confirmation message, the user should then choose Confirm or Cancel to either complete the logout or cancel the logout. 
If the user Clicks on the About link, it must open the about page in a new tab. It must NOT replace the current page

*Features*

1. Scrollbar on hover for the list of books
2. Search result for the search field input in the book description (apart from the book title and author)

- **Add a new bookpage (protected):**

The add new book page must be a simple form. This page should ONLY Be visible if the user has an active login session. 
It should contain the following fields in a form: Title, Description, Author(s), ISBN, Image URL, Stock Items and a button to cancel or a button to add the book.
The Add new book should also contain a “breadcrumb” at the top of the page. Please see the Wireframe for a visual example.

*Features*

1. Firm requirement for ISBN (must be a 13 digits number)

- **About page (public):**

This page will display information about the developers and software. It should have the name and emails of any developer that worked on the project. 
There should also be a section displaying the version, what technology was used (React or Angular) and the date the software was completed.

- **Edit user profile page (protected):**

The user can access this page from the Catalogue page. They may ONLY change their name not their email address.